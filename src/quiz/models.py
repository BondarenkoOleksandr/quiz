from django.db import models
from django.core.validators import MaxValueValidator
from accounts.models import User
from core.models import BaseModel

# Create your models here.
from core.utils import generate_uuid


class Test(BaseModel):
    QUESTION_MIN_LIMIT = 3
    QUESTION_MAX_LIMIT = 20

    class LEVEL_CHOICES(models.IntegerChoices):
        BASIC = 0, "Basic"
        MIDDLE = 1, "Middle"
        ADVANCED = 2, "Advanced"

    # topic = models.ForeignKey(to=Topic, related_name='tests', null=True, on_delete=models.SET_NULL)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    title = models.CharField(max_length=64)
    description = models.TextField(max_length=1024, null=True, blank=True)
    level = models.PositiveSmallIntegerField(choices=LEVEL_CHOICES.choices, default=LEVEL_CHOICES.MIDDLE)
    image = models.ImageField(default='default.png', upload_to='covers')

    def __str__(self):
        return f'{self.title}'

    def questions_count(self):
        return self.questions.count()

    def _get_best_result(self):
        if not self.results.all():
            return {'score': 'N/A', 'user': 'N/A'}
        result = self.results.first()
        for temp_result in self.results.all():
            if temp_result.num_incorrect_answers < result.num_incorrect_answers:
                result = temp_result
            elif temp_result.num_incorrect_answers == result.num_incorrect_answers:
                if temp_result.get_elapsed_time() < result.get_elapsed_time():
                    result = temp_result
        return {'score': result.num_correct_answers, 'user': result.user}

    def get_best_result_score(self):
        return self._get_best_result()['score']

    def get_best_result_user(self):
        return self._get_best_result()['user']

    def get_time_last_run(self):
        if self.results.last():
            return self.results.last().create_date
        return 'First start'

    def has_unfinished_run(self):
        return self.results.exclude(
            state=Result.STATE.FINISHED
        ).exists()

    @property
    def current_run(self):
        return self.results.exclude(
            state=Result.STATE.FINISHED
        ).first()


class Question(models.Model):
    ANSWER_MIN_LIMIT = 1
    ANSWER_MAX_LIMIT = 6

    test = models.ForeignKey(to=Test, related_name='questions', on_delete=models.CASCADE)
    order_number = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(Test.QUESTION_MAX_LIMIT)])
    text = models.CharField(max_length=64)

    def is_last(self, order_number):
        return order_number == self.test.questions_count()

    def __str__(self):
        return f'{self.text}'


class Choice(models.Model):
    text = models.CharField(max_length=64)
    question = models.ForeignKey(to=Question, related_name='choices', on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.text}'


class Result(BaseModel):
    class STATE(models.IntegerChoices):
        NEW = 0, "New"
        FINISHED = 1, "Finished"

    user = models.ForeignKey(to=User, related_name='results', on_delete=models.CASCADE)
    test = models.ForeignKey(to=Test, related_name='results', on_delete=models.CASCADE)
    state = models.PositiveSmallIntegerField(default=STATE.NEW, choices=STATE.choices)
    current_order_number = models.PositiveSmallIntegerField(null=True)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)

    num_correct_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )
    num_incorrect_answers = models.PositiveSmallIntegerField(
        default=0,
        validators=[
            MaxValueValidator(Test.QUESTION_MAX_LIMIT)
        ]
    )

    def __str__(self):
        return f'{self.test} = {self.get_points()}'

    def get_elapsed_time(self):
        a = self.write_date
        b = self.create_date
        return self.write_date - self.create_date

    def get_points(self):
        return max(0, self.num_correct_answers - self.num_incorrect_answers)

    def get_success_rate(self):
        return self.num_correct_answers/self.test.questions_count()*100

    def update_result(self, order_number, question, selected_choices):
        choices = question.choices.all()

        correct_answer = int(all(
            selected_choice == choices[i].is_correct
            for i, selected_choice in enumerate(selected_choices)
        ))

        self.num_correct_answers += correct_answer
        self.num_incorrect_answers += 1 - correct_answer
        self.current_order_number = question.order_number
        if question.is_last(order_number):
            self.state = self.STATE.FINISHED
        self.save()

    def save(self, *args, **kwargs):
        if self.state == self.STATE.FINISHED:
            self.user.rating += self.get_points()
            self.user.save()
        super().save(*args, **kwargs)