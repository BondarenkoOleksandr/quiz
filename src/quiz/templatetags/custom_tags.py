from django import template

register = template.Library()


def negate(value):
    return -value


def mult(value, arg):
    return value*arg


def div(value, arg):
    return value/arg


register.filter('negate', negate)
register.filter('mult', mult)
register.filter('div', div)
