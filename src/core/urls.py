from django.urls import path

from core import views

app_name = "cores"

url_patterns = [
    path('', views.index, 'index'),
]
